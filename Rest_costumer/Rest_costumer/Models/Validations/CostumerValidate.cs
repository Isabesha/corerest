﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Rest_costumer.Models
{
    [MetadataType(typeof(Costumer.MetaData))]
    public partial class Costumer
    {
        sealed class MetaData
        {

            [Key]
            public int Empid;

            [Required(ErrorMessage = "Se requiere el nombre")]
            public string Empname;

            [Required(ErrorMessage = "Se requiere el apellido")]
            public string Lastname;

            [Required]
            [EmailAddress(ErrorMessage = "Ingresa email válido")]
            public string Email;

            [Required]
            [Range(20,50,ErrorMessage = "Edad inválida")]
            public Nullable<int> Age;

            [Required(ErrorMessage = "*Campo obligatorio")]
            public string Adress;
        }
    }
}