﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Rest_costumer.Models;

namespace Rest_costumer.Controllers
{
    public class CostumersController : ApiController
    {
        private EMP2Entities db = new EMP2Entities();

        // GET: api/Costumers
        public IQueryable<Costumer> GetCostumer()
        {
            return db.Costumer;
        }

        // GET: api/Costumers/5
        [ResponseType(typeof(Costumer))]
        public IHttpActionResult GetCostumer(int id)
        {
            Costumer costumer = db.Costumer.Find(id);
            if (costumer == null)
            {
                return NotFound();
            }

            return Ok(costumer);
        }

        // PUT: api/Costumers/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCostumer(int id, Costumer costumer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != costumer.Empid)
            {
                return BadRequest();
            }

            db.Entry(costumer).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CostumerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Costumers
        [ResponseType(typeof(Costumer))]
        public IHttpActionResult PostCostumer(Costumer costumer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Costumer.Add(costumer);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = costumer.Empid }, costumer);
        }

        // DELETE: api/Costumers/5
        [ResponseType(typeof(Costumer))]
        public IHttpActionResult DeleteCostumer(int id)
        {
            Costumer costumer = db.Costumer.Find(id);
            if (costumer == null)
            {
                return NotFound();
            }

            db.Costumer.Remove(costumer);
            db.SaveChanges();

            return Ok(costumer);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CostumerExists(int id)
        {
            return db.Costumer.Count(e => e.Empid == id) > 0;
        }
    }
}