﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Core_empleados2.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Core_empleados2.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly ApplicationDbContext _db;

        public EmployeeController(ApplicationDbContext db)
        {
            _db = db;
        }




       [HttpGet]
        public async Task<IActionResult> Index(string empSearch)
        {
            ViewData["GetEmloyeesDetails"] = empSearch;
            var empquery = from x in _db.Employee select x;
            if (!String.IsNullOrEmpty(empSearch))
            {
                empquery = empquery.Where(x => x.Empname.Contains(empSearch) || x.Email.Contains(empSearch));
            }

            return View(await empquery.AsNoTracking().ToListAsync());
        }
       

        public IActionResult Create()
        {
            return View();  //Te regresa la vista  que se llama igual que el metodo
        }

        [HttpPost]
        public async Task<IActionResult> Create(Employee nEmp)  //Este metodo nos lleva a llevar el formulario para agregar un empleado a la base de datos
        {
            if(ModelState.IsValid)
            {
                _db.Add(nEmp);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");  // si el empleado fue ingesado exitosamente, nos lleva a la pagina principal

            }

            return View(nEmp);  //En caso de haber una falla, se mantiene en la vista del formulario sin que re borren los datos ya ingresados.
        }

        public async Task<ActionResult> Detail (int? id) //Se pone ? porque puede que haya o no id.
        {
            if (id == null)
            {
                return RedirectToAction("Index");

            }

            var getEmpDetail = await _db.Employee.FindAsync(id);
            return View(getEmpDetail);
        }


        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var getEmpDetail = await _db.Employee.FindAsync(id);
            return View(getEmpDetail);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Employee oldEmp)
        {
            if (ModelState.IsValid)
            {
                _db.Update(oldEmp);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(oldEmp);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var getEmpDetail = await _db.Employee.FindAsync(id);
            return View(getEmpDetail);
        }

        [HttpPost]
        public async Task<IActionResult> Delete (int id)
        {
            var getEmpDetail = await _db.Employee.FindAsync(id);
            _db.Employee.Remove(getEmpDetail);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}
