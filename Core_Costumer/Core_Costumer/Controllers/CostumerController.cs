﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core_Costumer.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Core_Costumer.Controllers
{
    public class CostumerController : Controller
    {

        private readonly ApplicationDbContext _db;

        public CostumerController(ApplicationDbContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
            var displaydata = _db.Customer.ToList();   //Custumer es el nombre de la tabla para entrar a los datos que almacena en ella. por medio del objeto _db que hace la conexión
            return View(displaydata);
        }
                                                        //Estos dos son los mismos métodos, pero sobrecargados, si elimino el primero, aún así funciona.
        [HttpGet]
        public async Task<IActionResult> Index(string empSearch)
        {
            ViewData["GetCostumersDetails"] = empSearch;//Dato compartido con la vista
            var empquery = from x in _db.Customer select x; //Query de EF, de la tabla Employee almacena en x
            if (!String.IsNullOrEmpty(empSearch))
            {
                empquery = empquery.Where(x => x.Empname.Contains(empSearch) || x.Email.Contains(empSearch)); //Agrega claususla Where
            }
            return View(await empquery.AsNoTracking().ToListAsync()); //Retorna el resultado del query como una lista asincrona que no pasa por _db
        }

        public IActionResult Create()
        {
            return View();  //Te regresa la vista  que se llama igual que el metodo
        }

        [HttpPost]
        public async Task<IActionResult> Create(Costumer nEmp)  //Este metodo nos lleva a llevar el formulario para agregar un empleado a la base de datos
        {
            if (ModelState.IsValid)
            {
                _db.Add(nEmp);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");  // si el empleado fue ingesado exitosamente, nos lleva a la pagina principal

            }

            return View(nEmp);  //En caso de haber una falla, se mantiene en la vista del formulario sin que re borren los datos ya ingresados.
        }

        public async Task<ActionResult> Detail(int? id) //Se pone ? porque puede que haya o no id.
        {
            if (id == null)
            {
                return RedirectToAction("Index");

            }

            var getEmpDetail = await _db.Customer.FindAsync(id);
            return View(getEmpDetail);
        }


        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var getEmpDetail = await _db.Customer.FindAsync(id);
            return View(getEmpDetail);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Costumer oldEmp)
        {
            if (ModelState.IsValid)
            {
                _db.Update(oldEmp);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(oldEmp);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var getEmpDetail = await _db.Customer.FindAsync(id);
            return View(getEmpDetail);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var getEmpDetail = await _db.Customer.FindAsync(id);
            _db.Customer.Remove(getEmpDetail);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }


    }
}
