﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Core_Costumer.Models
{
    public class Costumer
    {
        [Key]
        public int Empid { get; set; }

        [Required(ErrorMessage = "Ingrese el nombre")]
        [Display(Name = "Nombre")]
        public string Empname { get; set; }

        [Required(ErrorMessage = "Ingrese el apellido")]
        [Display(Name = "Apellido")]
        public string Lastname { get; set; }

        [Required(ErrorMessage = "Ingrese el email")]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Dirección de email inválido")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Ingrese la edad")]
        [Display(Name = "Edad")]
        [Range(20, 50)]
        public int Age { get; set; }

        [Required(ErrorMessage = "Ingrese la dirección")]
        [Display(Name = "Dirección")]
        public string Adress
        {
            get; set;
        }

    }
}
