﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core_Costumer.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }
        public DbSet<Costumer> Customer { get; set; } // el segundo Customer es el nombre de la tabla de la base de datos
        //El Costumer entre diamantes, es el nombre de mi clase
    }
}
