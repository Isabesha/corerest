﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Rest_employee.Models
{

    [MetadataType(typeof(Employee.MetaData))]
    public partial class Employee
    {

        sealed class MetaData
        {
            [Key]
            public int Empid;

            [Required (ErrorMessage = "Se requiere el nombre")]
            public string Empname;

            [Required]
            [EmailAddress(ErrorMessage ="Ingresa email válido")]
            public string Email;

            //[Required]
            //[Range(20, 50, 
            public Nullable<int> Age;
            public Nullable<int> Salary;

        }
    }
}